from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
    name: example
    plugin_type: inventory
    author:
      - matt fernandez
    short_description: Dynamic inventory plugin example
    version_added: "n/a"
    extends_documentation_fragment:
        - constructed
    options:
        plugin:
            description: Token that ensures this is a source file for the plugin.
            required: true
            choices: ['example']
        parameter:
            description: something to pass
            required: true
            type: string
            env:
              - name: EXAMPLE_PARAMETER
    requirements:
        - python >= 3.8
'''

from ansible.plugins.inventory import BaseFileInventoryPlugin, Constructable
from ansible.errors import AnsibleError
from distutils.version import LooseVersion


try:
    import requests
    if LooseVersion(requests.__version__) < LooseVersion('1.1.0'):
        raise ImportError
except ImportError:
    raise AnsibleError('This plugin requires python-requests 1.1 as a minimum version')


class InventoryModule(BaseFileInventoryPlugin, Constructable):

    NAME = 'example'

    def verify_file(self, path):
      super(InventoryModule, self).verify_file(path)
      return path.endswith(('example.yml', 'example.yaml'))

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)
        self._read_config_data(path)

        self.inventory.add_host('test1')
        self.inventory.add_host('test2')

        self.inventory.set_variable('test1', 'name', 'test1')
        self.inventory.set_variable('test1', 'example_ipv4', '172.16.0.1')
        self.inventory.set_variable('test2', 'name', 'test2')
        self.inventory.set_variable('test2', 'example_ipv4', '172.16.0.2')
