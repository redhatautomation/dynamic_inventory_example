## CLI Testing
```
ansible-inventory -i example.yaml  --list --playbook-dir .
```
## AAP Config
![AAP Configuration](./example_aap_config.png)

## documentation
[Open source ansible documentation on developing inventory plugins](https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#inventory-plugins)

[AAP administration documentation](https://docs.ansible.com/automation-controller/latest/html/administration/)

